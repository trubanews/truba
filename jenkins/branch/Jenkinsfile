pipeline {
  agent any
  options { 
    disableConcurrentBuilds()
    buildDiscarder(logRotator(daysToKeepStr: "-1", numToKeepStr: "1", artifactDaysToKeepStr: "-1", artifactNumToKeepStr: "-1"))
  }
  environment {
    DOCKER_REGISTRY = 'registry.truba.news'
  }
  stages {
    stage('core') {
      when {
          anyOf {
              changeset "${STAGE_NAME}/**"
              changeset "shared/**"
          }
      }
      steps {
        script {
            docker.withRegistry("https://${env.DOCKER_REGISTRY}", 'docker-credentials') {
              def image = docker.build("${STAGE_NAME}:${BRANCH_NAME}", """ . -f "./${STAGE_NAME}/Dockerfile" """)
              image.push()
            }
        }
        withKubeConfig([credentialsId: 'kubeconfig-credentials']) {
            sh """kubectl rollout restart deployment ${STAGE_NAME} -n "${BRANCH_NAME}" """
            sh """kubectl rollout restart deployment coreui -n "${BRANCH_NAME}" """
            sh """kubectl rollout restart deployment worker -n "${BRANCH_NAME}" """
        }
      }
    }
    stage('ml') {
      when {
          anyOf {
              changeset "${STAGE_NAME}/**"
              changeset "shared/**"
          }
      }
      steps {
        script {
            docker.withRegistry("https://${env.DOCKER_REGISTRY}", 'docker-credentials') {
              def image = docker.build("${STAGE_NAME}:${BRANCH_NAME}", """ . -f "./${STAGE_NAME}/Dockerfile" """)
              image.push()
            }
        }
        withKubeConfig([credentialsId: 'kubeconfig-credentials']) {
            sh """kubectl rollout restart deployment ${STAGE_NAME} -n "${BRANCH_NAME}" """
        }
      }
    }
    stage('client') {
      when { changeset "${STAGE_NAME}/**" }
      steps {
        script {
            docker.withRegistry("https://${env.DOCKER_REGISTRY}", 'docker-credentials') {
              def image = docker.build("${STAGE_NAME}:${BRANCH_NAME}", """ . -f "./${STAGE_NAME}/Dockerfile" --build-arg ENVIRONMENT=${BRANCH_NAME} """)
              image.push()
            }
        }
        withKubeConfig([credentialsId: 'kubeconfig-credentials']) {
            sh """kubectl rollout restart deployment ${STAGE_NAME} -n "${BRANCH_NAME}" """
        }
      }
    }
    stage('scraper') {
      when {
          anyOf {
              changeset "${STAGE_NAME}/**"
              changeset "shared/**"
          }
      }
      steps {
        script {
            docker.withRegistry("https://${env.DOCKER_REGISTRY}", 'docker-credentials') {
              def image = docker.build("${STAGE_NAME}:${BRANCH_NAME}", """ . -f "./${STAGE_NAME}/Dockerfile" """)
              image.push()
            }
        }
      }
    }
  }
  post{
    always {
      sh "docker image prune -f"
    }
  }
}