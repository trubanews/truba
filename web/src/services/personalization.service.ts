import { userService } from "./user.service";
import { FavoriteItem, Recommendation } from "../models";
import { User } from "../models/user.model";
import { appConfig } from "../app.config";
import { httpService } from "./http.service";

let personalization: Recommendation | null;

export const personalizationService = {
  personalization: () => personalization,
  getPersonalization,
  postUpdatePersonalization,
  postFeedback,
  toggleFavorite,
};

function getPersonalization(user: User): Promise<Recommendation> {
  const lang = user.language ? user.language : "en";
  return httpService
    .get<Recommendation>(appConfig.backendApi + "user/info/" + lang)
    .then((pers) => {
      personalization = new Recommendation(pers);
      return personalization;
    });
}

function postUpdatePersonalization(
  type: string,
  favoriteItem: FavoriteItem
): Promise<any> {
  return httpService.post<any>(appConfig.backendApi + type, favoriteItem);
}

function postFeedback(
  searchTerm: string,
  storyId: string,
  feedBackType: number
): void {
  const currentUser = userService.getUser();
  if (!currentUser) return;
  const postData = {
    user_id: currentUser.id,
    search_term: searchTerm,
    story_id: storyId,
    feedback_type: feedBackType,
  };
  httpService.post<any>(appConfig.backendApi + "feedback", postData);
}

function toggleFavorite(item: FavoriteItem, type: string): Promise<any> {
  if (item.relevancy_rate < 0.0) {
    item.relevancy_rate = 1.0;
  }
  item.is_favorite = !item.is_favorite;
  if (!item.is_favorite) {
    item.relevancy_rate = 0.0;
  }
  const u = userService.getUser();
  if (u) item.user_id = u.id;
  return postUpdatePersonalization(type, item);
}
