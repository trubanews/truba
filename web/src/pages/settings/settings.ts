import { LitElement, html } from "lit";
import { customElement, query } from "lit/decorators.js";
import { TranslationController } from "../../controllers";
import { userService, personalizationService } from "../../services";
import { Recommendation, FavoriteItem, IdValuePair } from "../../models";
import { textFieldStyles, iconButtonStyles, chipStyles } from "../../styles";
import { FavoriteChips } from "../../components/favorite-chips/favorite-chips";
import { styles } from "./settings.styles";

import "../../components/favorite-chips/favorite-chips";
import "@material/mwc-formfield";
import "@material/mwc-textfield";


@customElement("app-settings")
class Settings extends LitElement {
  static styles = [styles, textFieldStyles, chipStyles, iconButtonStyles];

  private i18n = new TranslationController(this);

  private availableKeywords: FavoriteItem[] = [];
  private availableEntities: FavoriteItem[] = [];
  private availableSources: FavoriteItem[] = [];
  private availableAuthors: FavoriteItem[] = [];
  private personalization: Recommendation;

  @query("#keywords")
  keywords: FavoriteChips;

  @query("#keywords-input")
  keywordInput: HTMLInputElement;

  @query("#keyword-hated")
  hatedKeywords: FavoriteChips;

  @query("#entity-hated")
  hatedEntities: FavoriteChips;

  @query("#author-hated")
  hatedAuthors: FavoriteChips;

  @query("#source-hated")
  hatedSources: FavoriteChips;

  constructor() {
    super();
    const user = userService.getUser();
    if (user) {
      personalizationService
        .getPersonalization(user)
        .then((personalization) => {
          this.initAvailableItems(personalization);
        });
    } else {
      userService
        .me()
        .then((user) => {
          return personalizationService.getPersonalization(user);
        })
        .then((personalization) => {
          this.initAvailableItems(personalization);
        });
    }
  }

  render() {
    return html`
      <div class="settings-container">
        <div class="settings-wrap">
          <div class="type-box">
            ${this.createKeywordChips()} 
            ${this.hatedChips("keyword", this.personalization?.hated_items?.favorite_keywords || [])}
          </div>
          <div class="type-box">
            ${this.createEntityChips()}
            ${this.hatedChips("entity", this.personalization?.hated_items?.favorite_entities || [])}
          </div>
          <div class="type-box">
            ${this.createSourcesChips()}
            ${this.hatedChips("source", this.personalization?.hated_items?.favorite_sources || [])}
          </div>
          <div class="type-box">
            ${this.createAuthorsChips()}
            ${this.hatedChips("author", this.personalization?.hated_items?.favorite_authors  || [])}
          </div>
        </div>
      </div>
    `;
  }

  private createKeywordChips() {
    return html`
      <favorite-chips
        .favorite_items=${this.availableKeywords}
        id="keywords"
        type="keyword"
        canRemove
        @removed=${(e: CustomEvent) => {
          this.hatedKeywords.favorite_items?.push(e.detail)
          this.hatedKeywords.requestUpdate();
          }}
      ></favorite-chips>
      <mwc-textfield
        id="keywords-input"
        type="text"
        name="keywords"
        placeholder=${this.i18n.t("home:addKeyword")}
        .disabled=${this.availableKeywords.filter(item => item.is_favorite).length >= 10}
        @keydown=${this.addKeyword}
      ></mwc-textfield>
    `;
  }

  private createEntityChips() {
      return html` 
        <favorite-chips
          .favorite_items=${this.availableEntities}
          type="entity"
          canRemove
          @removed=${(e: CustomEvent) => {
            this.hatedEntities.favorite_items?.push(e.detail)
            this.hatedEntities.requestUpdate();
          }}
        ></favorite-chips>
      `;
  }

  private createSourcesChips() {
    return html` 
      <favorite-chips
        .favorite_items=${this.availableSources}
        type="source"
        canRemove
        @removed=${(e: CustomEvent) => {
          this.hatedSources.favorite_items?.push(e.detail)
          this.hatedSources.requestUpdate();
        }}
      ></favorite-chips>
    `;
  }

  private createAuthorsChips() {
    return html` 
      <favorite-chips
        .favorite_items=${this.availableAuthors}
        type="author"
        canRemove
        @removed=${(e: CustomEvent) => {
          this.hatedAuthors.favorite_items?.push(e.detail)
          this.hatedAuthors.requestUpdate();
        }}
      ></favorite-chips>
    `;
  }

  private hatedChips(type: string, hatedItems: FavoriteItem[]) {
    return html` 
      <favorite-chips
        id=${type + "-hated"}
        .favorite_items=${hatedItems}
        type=${type}
        canRemove
        isHated
      ></favorite-chips>
    `;
  }


  initAvailableItems(personalization: Recommendation) {
    this.personalization = personalization;
    this.availableKeywords = this.setAvailableItems(
      personalization.favorite_items.favorite_keywords,
      personalization.recommended_items.favorite_keywords
    );

    this.availableEntities = this.setAvailableItems(
      personalization.favorite_items.favorite_entities,
      personalization.recommended_items.favorite_entities
    );

    this.availableSources = this.setAvailableItems(
      personalization.favorite_items.favorite_sources,
      personalization.recommended_items.favorite_sources
    );

    this.availableAuthors = this.setAvailableItems(
      personalization.favorite_items.favorite_authors,
      personalization.recommended_items.favorite_authors
    );
    this.requestUpdate();
  }

  setAvailableItems(
    favorite_items: FavoriteItem[],
    recommended_items: FavoriteItem[]
  ): FavoriteItem[] {
    const recommended = recommended_items.filter(
      (rec) => !favorite_items.some((fav) => fav.identifier === rec.identifier)
    );
    return favorite_items.concat(recommended);
  }

  addKeyword(event: KeyboardEvent): void {
    const value = this.keywordInput.value;
    if (value && event.code == "Enter" &&
      this.availableKeywords.filter(item => item.is_favorite).length < 10) {
      const pair: IdValuePair = {
        id: value.trim(),
        value: value.trim(),
      };
      const item = new FavoriteItem(pair, userService.getUser()!);
      item.is_favorite = true;
      item.is_added = true;
      personalizationService.postUpdatePersonalization("keyword", item).then(() => {
        this.availableKeywords.filter(i => i.value !== item .value)
        this.availableKeywords.push(item);
        this.keywordInput.value = "";
        this.keywords.requestUpdate();
        this.requestUpdate();
      })
    }
  }
}
