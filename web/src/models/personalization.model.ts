import { FavoriteItem } from "./favorite-item.model";

export class Personalization {
  favorite_keywords: FavoriteItem[];
  favorite_entities: FavoriteItem[];
  favorite_sources: FavoriteItem[];
  favorite_authors: FavoriteItem[];
}

export class ExcludeItems {
  keywords: string[]
  entities: string[]
  sources: string[]
  authors: string[]
}

export class Recommendation {
  recommended_items: Personalization;
  favorite_items: Personalization;
  hated_items: Personalization;

  constructor(recommendation: any) {
    this.recommended_items = this.mapFavoriteItems(
      recommendation.recommended_items
    );
    this.favorite_items = this.mapFavoriteItems(recommendation.favorite_items);
    this.hated_items = this.mapFavoriteItems(recommendation.hated_items);
  }

  getExcludedItems(): ExcludeItems {
    return {
      keywords: this.hated_items.favorite_keywords.map(x => x.identifier),
      entities: this.hated_items.favorite_entities.map(x => x.identifier),
      sources: this.hated_items.favorite_sources.map(x => x.identifier),
      authors: this.hated_items.favorite_authors.map(x => x.identifier),
    };
  }

  private mapFavoriteItems(personalization: any): Personalization {
    return {
      favorite_keywords: personalization.favorite_keywords,
      favorite_entities: personalization.favorite_entities,
      favorite_sources: personalization.favorite_sources,
      favorite_authors: personalization.favorite_authors,
    };
  }
  
}
