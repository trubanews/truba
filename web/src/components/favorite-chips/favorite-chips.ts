import { LitElement, html } from "lit";
import { customElement, property } from "lit/decorators.js";
import { classMap } from "lit-html/directives/class-map.js";
import { personalizationService } from "../../services";
import { chipStyles } from "../../styles";
import { FavoriteItem } from "../../models";
import { TranslationController } from "../../controllers";
import { styles } from "./favorite-chips.styles";

@customElement("favorite-chips")
class FavoriteChipsComponent extends LitElement {
  static styles = [styles, chipStyles];

  private i18n = new TranslationController(this);

  @property({ type: Array })
  favorite_items?: FavoriteItem[];

  @property({ type: String })
  type: string;

  @property({ type: Boolean })
  canRemove = false;

  @property({ type: Boolean })
  isHated = false;

  constructor() {
    super();
  }

  render() {
    if (!this.favorite_items || this.favorite_items.length === 0) return "";
    return html` <div>
      <div class="title">${this.isHated ? "" : this.i18n.t(`home:${this.type}`)}</div>
      ${this.renderSubTitle()}
      <div class="chip-set">
        ${this.favorite_items?.map((item) => { 
          const classes = { selected: item.is_favorite, error: item.is_deleted };
          return html` <div
            class="md-chip ${classMap(classes)}"
          >
            <div
              class="md-chip-label"
              @click="${(e: Event) => this.toggleFavorite(item, this.type)}"
            >
              ${item.value}
            </div>
            ${
              this.canRemove ? html`
              <i
                class="material-icons mdc-icon-button__icon"
                @click="${(e: Event) => this.removeFavorite(item, this.type)}"
                >close</i>` 
              : ""
            }
          </div>`;
        })}
      </div>
    </div>`;
  }

  renderSubTitle() {
    if (this.canRemove) {
      return this.isHated ? 
        html`<div class="sub-title">${this.i18n.t(`home:excluded`)}</div>` : 
        html`<div class="sub-title">${this.i18n.t(`home:preferred`)}</div>`;
    }
    return ""
  }

  toggleFavorite(item: FavoriteItem | undefined, type: string) {
    if (!item || this.isHated) return;
    personalizationService.toggleFavorite(item, type).then(() => {
      this.requestUpdate();
    });
  }

  removeFavorite(item: FavoriteItem, type: string): void {
    if (item.is_favorite) {
      item.is_favorite = false;
    }
    if (this.isHated) {
      item.is_deleted = false;
    } else {
      item.is_deleted = true;
    }
    item.relevancy_rate = 0.0;
    personalizationService.postUpdatePersonalization(type, item).then(() => {
      this.favorite_items = this.favorite_items?.filter(
        (x) => x.identifier !== item.identifier
      );
      
      const options = {
        detail: item,
        bubbles: true,
        composed: true
      }
      this.dispatchEvent(new CustomEvent('removed', options));
      this.requestUpdate();
    });
  }
}

export class FavoriteChips extends FavoriteChipsComponent {}
